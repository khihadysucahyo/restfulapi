<?php

namespace App\Http\Controllers\Seller;

use App\Seller;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Storage;
use App\Transformers\ProductTransformer;
use Illuminate\Auth\Access\AuthorizationException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SellerProductController extends ApiController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('scope:manage-products')->except('index');
        $this->middleware('transform.input:' . ProductTransformer::class)->only(['store', 'update']);
        $this->middleware('can:view,seller')->only('index');
        $this->middleware('can:sale,seller')->only('store');
        $this->middleware('can:edit-product,seller')->only('update');
        $this->middleware('can:delete-product,seller')->only('delete');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Seller $seller)
    {
        if (request()->user()->tokenCan('read-general') || request()->user()->tokenCan('manage-products'))
        {
            $products = $seller->products;

            return $this->showAll($products);
        }

        throw new AuthorizationException;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Seller $seller, Request $request)
    {

        $rules = [
            'name' => 'required|min:3',
            'description' => 'required|min:10',
            'quantity' => 'required|integer|min:1',
            'image' => 'required|image'
        ];

        $request->validate($rules);

        $data = $request->all();

        $data['status'] = Product::UNAVAILABLE_PRODUCT;
        $data['image'] = $request->image->store('');
        $data['seller_id'] = $seller->id;

        $product = Product::create($data);

        return $this->showOne($product, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Seller $seller, Product $product)
    {
        $this->checkSeller($seller, $product);

        return $this->showOne($product); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Seller $seller, Product $product, Request $request)
    {   

        $rules = [
            'name' => 'min:3',
            'description' => 'min:10',
            'quantity' => 'integer|min:1',
            'status' => 'in:' . Product::AVAILABLE_PRODUCT . ',' . Product::UNAVAILABLE_PRODUCT
        ];

        $request->validate($rules);

        $this->checkSeller($seller, $product);

        $product->fill($request->only([
            'name',
            'description',
            'quantity'
        ]));

        if ($request->has('status'))
        {
            $product->status = $request->status;

            if ($product->isAvailable() && $product->categories()->count() == 0)
            {
                return $this->errorResponse(
                    'An active product must have at least one category.',
                    409
                );
            }
        }

        if ($request->hasFile('image'))
        {
            Storage::delete($product->image);

            $product->image = $request->image->store('');
        }

        if ($product->isClean())
        {
            return $this->errorResponse(
                'You need specify any different value to update.',
                422
            );
        }

        $product->save();

        return $this->showOne($product, 202);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Seller $seller, Product $product, Request $request)
    {
        $this->checkSeller($seller, $product);

        $product->delete();
        
        Storage::delete($product->image);

        return $this->showOne($product);
    }

    protected function checkSeller($seller, $product)
    {
        if ($seller->id != $product->seller_id)
        {
            throw new HttpException(
                422,
                'The specified seller is not the actual seller of the product'
            );
        }
        return;
    }
}
