<?php

use App\Transaction;
use Illuminate\Database\Seeder;

class TransactionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = 1000;
        Transaction::truncate();
        Transaction::flushEventListeners();

        factory(Transaction::class, $count)->create();
    }
}
