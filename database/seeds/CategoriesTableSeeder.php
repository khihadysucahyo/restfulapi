<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = 30;
        Category::truncate();
        Category::flushEventListeners();

        factory(Category::class, $count)->create();
    }
}
