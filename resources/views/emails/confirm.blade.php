@component('mail::message')
Hello {{ $user->name }}

You changed your email, so we need to verify this new address.

@component('mail::button', ['url' => route('verify', $user->verification_token)])
Verify
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
